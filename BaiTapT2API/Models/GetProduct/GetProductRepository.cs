﻿using BaiTapT2API.Models;

namespace BaiTapT2API.Models.GetProduct
{
    public interface IGetProductRepository
    {
        Task<IEnumerable<Product>> GetProductsAsync();
    }
}