﻿using BaiTapT2API.Models.GetProduct;
using BaiTapT2API.Models;
using Microsoft.EntityFrameworkCore;

namespace BaiTapT2API.Models.GetProduct
{
    public class GetProductRepository : IGetProductRepository
    {
        private readonly ApplicationDbContext _context;
        public GetProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return await _context.Products.ToListAsync();
        }
    }
}